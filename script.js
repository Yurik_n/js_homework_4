/*
Теоретичні питання

1) Описати своїми словами навіщо потрібні функції у програмуванні.
2) Описати своїми словами, навіщо у функцію передавати аргумент.
3) Що таке оператор return та як він працює всередині функції?

1) Функції - це блок коду, який можна повторно використовувати n-у кількість разів, не копіюючи сам блок коду, 
та викликати з будь-якого місця програми 
2) За допомогою аргументів можемо передавати в функції різні значення, ці значення можна по-різному обробляти 
в залежності від очікуванного результату 
3) Щоб функція повертала яке-небудь значення (string, number, boolean тощо) для подальшого його використання, 
наприклад зберегти у змінну, - застосовують оператор return
*/

let getFirstNumber = prompt("Enter first number")
while (getFirstNumber === null || getFirstNumber === "" || Number.isNaN(+getFirstNumber)) {
    getFirstNumber = prompt("Enter your first number again")
}

let getSecondNumber = prompt("Enter second number")
while (getSecondNumber === null || getSecondNumber === "" || Number.isNaN(+getSecondNumber)) {
    getSecondNumber = prompt("Enter your second number again")
}

let getSign = prompt("Select one sign +,  -,  *,  / ")
while (getSign !== '+' && getSign !== '-' && getSign !== '/' && getSign !== '*') {
    getSign = prompt("Select your sign again  +,  -,  *,  / ")
}

function result(num1, num2, sign) {
    switch (sign) {
        case "+":
            return Number(num1) + Number(num2)            
        case "-":
            return num1 - num2            
        case "*":
            return num1 * num2            
        case "/":
            return num1 / num2            
        default:
            break;
    }
}

console.log(`Результат : ${result(getFirstNumber, getSecondNumber, getSign)}`)


